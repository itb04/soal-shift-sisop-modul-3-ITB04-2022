# soal-shift-sisop-modul-3-ITB04-2022
## Laporan Pengerjaan Soal Shift Modul 3 Praktikum Sistem Operasi

Nama Anggota Kelompok:
 1. Hafizh Abid Wibowo - 5027201011
 2. Axellino Anggoro Armandito - 5027201040
 3. Anak Agung Bintang - 5027201060


# Daftar Isi
- [soal-shift-sisop-modul-3-ITB04-2022](#soal-shift-sisop-modul-3-itb04-2022)
	- [Laporan Pengerjaan Soal Shift Modul 3 Praktikum Sistem Operasi](#laporan-pengerjaan-soal-shift-modul-3-praktikum-sistem-operasi)
- [Daftar Isi](#daftar-isi)
- [Soal 1](#soal-1)
	- [Soal 1a](#soal-1a)
		- [Output 1a](#output-1a)
	- [Soal 1b](#soal-1b)
		- [Output 1b](#output-1b)
	- [Soal 1c](#soal-1c)
	- [Soal 1d](#soal-1d)
		- [Output 1d](#output-1d)
	- [Soal 1e](#soal-1e)
		- [Output 1e](#output-1e)
- [Kendala pengerjaan](#kendala-pengerjaan)
	- [Soal 1](#soal-1-1)

# Soal 1
### Soal 1a
Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama **quote** untuk file zip quote.zip dan **music** untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.

<br>

Pertama, untuk mendownload file zip dilakukan dengan command berikut:
```c
if(fork() == 0) {
		char *argv[] = {"wget", "-q", "--no-check-certificate", "https://drive.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1", "-O", "music.zip", NULL};
		execv("/usr/bin/wget", argv);
	}
	wait(NULL);

	if(fork() == 0) {
		char *argv[] = {"wget", "-q" ,"--no-check-certificate", "https://drive.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt", "-O", "quote.zip", NULL};
		execv("/usr/bin/wget", argv);
	}
	wait(NULL);

	sleep(3);
```

- `wget` digunakan untuk melakukan download pada kedua file zip
- `execv` digunakan untuk mengexecute perintah download file
- `wait(NULL)` sebagai perintah untuk menunggu child proses selesai dipanggil
- `sleep(3)` untuk melakukan delay pada program sebanyak 3 detik sehingga proses download bisa diselesaikan terlebih dahulu

<br>

Selanjutnya, untuk melakukan unzip secara bersamaan menggunakan thread pada kedua file tadi, kami membuat fungsi bernama `unzip` sebagai berikut: 

```c
void* unzip(void *arg)
{
	char *argv1[] = {"unzip", "-qo", "music.zip", "-d", "./music/", NULL};
	char *argv2[] = {"unzip", "-qo", "quote.zip", "-d", "./quote/", NULL};
	pthread_t id = pthread_self();
	if(pthread_equal(id, tid[0])) {
		if (fork() == 0) {
            execv("/usr/bin/unzip", argv1);
        }
		wait(NULL);
	}
	else if(pthread_equal(id, tid[1])) {
        if (fork() == 0) {
            execv("/usr/bin/unzip", argv2);
        }
		wait(NULL);
	}
	return NULL;
}
```
- `char *argv1[]` digunakan untuk menjalankan fungsi unzip pada file **music.zip**. Sedangkan `char *argv2[]` digunakan untuk menjalankan fungsi unzip pada file **quote.zip**
- `pthread_t id = pthread_self()` digunakan untuk inisialisasi unique identifier pada setiap thread id
- `if(pthread_equal(id, tid[0])` pada statement if ini digunakan untuk menjalankan thread id 0 agar melakukan unzip pada file **music.zip**. Sedangkan pada statement if `else if(pthread_equal(id, tid[1]))` digunakan untuk menjalankan thread id 1 agar melakukan unzip pada file **quote.zip**

<br>

Agar fungsi `unzip` tadi dapat berjalan, kita perlu memanggilnya menggunakan `while loop` sebanyak jumlah thread yang dibutuhkan sebagai berikut:

```c
int i = 0;
int err;
while(i<2)
	{
		err=pthread_create(&(tid[i]), NULL, &unzip, NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
		i++;
	}
	pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);
```
Pada iterasi pertama akan membuat thread baru dengan id 0 yang di assign untuk melakukan unzip pada file **music.zip**. Setelah thread pertama berhasil dibuat maka *i* akan diincrement menjadi 1 dan masuk kembali ke while loop untuk membuat thread dengan id 1 yang di assign untuk melakukan unzip pada file **quote.zip**. Setelah diincrement *i* akan bernilai 2 sehingga keluar dari loop dan masuk ke fungsi `pthread_join(tid[0],NULL)` dan `pthread_join(tid[1],NULL)` untuk menggabungkan dengan thread lain yang telah menyelesaikan pekerjaannya.

#### Output 1a
![output_1a](img/1a.png)

### Soal 1b
Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.

<br>

Untuk melakukan decode base 64 menggunakan library `base64_decode` yang ditemukan di github sebagai berikut:
```c
char base46_map[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                     'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                     'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                     'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

char* base64_decode(char* cipher) {

    char counts = 0;
    char buffer[4];
    char* plain = malloc(strlen(cipher) * 3 / 4);
    int i = 0, p = 0;

    for(i = 0; cipher[i] != '\0'; i++) {
        char k;
        for(k = 0 ; k < 64 && base46_map[k] != cipher[i]; k++);
        buffer[counts++] = k;
        if(counts == 4) {
            plain[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if(buffer[2] != 64)
                plain[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if(buffer[3] != 64)
                plain[p++] = (buffer[2] << 6) + buffer[3];
            counts = 0;
        }
    }

    plain[p] = '\0';    /* string padding character */
    return plain;
}
```

<br>

Selanjutnya, untuk melakukan decode pada isi direktori **quote** dan **music** kami menggunakan template directory listing yang ada di modul 2 sebagai fungsi `decode` berikut:
```c
void* decode(void *arg)
{
	pthread_t id = pthread_self();
	if(pthread_equal(id, tid2[0])) {
		DIR *dp;
		struct dirent *ep;
		char path[100];

		sprintf(path, "./music/");

		dp = opendir(path);
		FILE *file, *txt;

		if (dp != NULL)
		{
		while ((ep = readdir (dp))) {
			char str[100] = "";
			char filePath[1000] = "";
			if(strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
				sprintf(filePath, "./music/%s", ep->d_name);
				file = fopen(filePath, "r");
				while(fgets(str, 100, file) != NULL) {
					txt = fopen("music.txt", "a");
					fprintf(txt, "%s\n", base64_decode(str));
					fclose(txt);
				}
				fclose(file);	
			}
		}

		(void) closedir (dp);
		} else perror ("Couldn't open the directory");
	}
	else if(pthread_equal(id, tid2[1])) {
        DIR *dp;
		struct dirent *ep;
		char path[100];

		sprintf(path, "./quote/");

		dp = opendir(path);
		FILE *file, *txt;

		if (dp != NULL)
		{
		while ((ep = readdir (dp))) {
			char str[100] = "";
			char filePath[1000] = "";
			if(strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
				sprintf(filePath, "./quote/%s", ep->d_name);
				file = fopen(filePath, "r");
				while(fgets(str, 100, file) != NULL) {
					txt = fopen("quote.txt", "a");
					fprintf(txt, "%s\n", base64_decode(str));
					fclose(txt);
				}
				fclose(file);	
			}
		}

		(void) closedir (dp);
		} else perror ("Couldn't open the directory");
	}
	return NULL;
}
```
Pada thread id yang pertama akan membuka folder dari direktori **music** kemudian membaca setiap baris yang terdapat string dari file m1.txt sampai m9.txt. Selanjutnya akan dibuat file **music.txt** untuk menampung hasil decode setiap line dari keseluruhan file. Setelah semua file di decode terdapat fungsi `closedir` untuk menyudahi listing directory. Pada thread id kedua memiliki cara kerja yang sama dengan pertama, tetapi akan bekerja pada direktori **quote**.

<br>

```c
while(x<2)
	{
		err=pthread_create(&(tid2[x]), NULL, &decode, NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
		x++;
	}
```
Untuk memanggil fungsi `decode` tadi, digunakan while loop agar dapat melakukan iterasi pada setiap pembuatan thread yang berhasil.

#### Output 1b

> Untuk music.txt

![output_1b_music](img/1b_music.png)

> Untuk quote.txt

![output_1b_music](img/1b_quote.png)

### Soal 1c
Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama `hasil`.

<br>

```c
if(fork() == 0) {
		char *argv[] = {"mv", "music.txt", "hasil/music.txt", NULL};
		execv("/usr/bin/mv", argv);
	}
	wait(NULL);

	wait(NULL);

if(fork() == 0) {
		char *argv[] = {"mv", "quote.txt", "hasil/quote.txt", NULL};
		execv("/usr/bin/mv", argv);
	}
	wait(NULL);
```
- `if(fork() == 0)` Untuk menandakan program berjalan pada child proses
- `char *argv[] = {"mv", "music.txt", "hasil/music.txt", NULL}` dan `char *argv[] = {"mv", "quote.txt", "hasil/quote.txt", NULL}` untuk memindahkan music.txt dan quote.txt ke folder hasil
- `execv("/usr/bin/mv", argv)` untuk melakukan execute pada perintah move
- `wait(NULL)` untuk menunggu proses selesai sebelum berpindah ke proses selanjutnya

### Soal 1d
Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak).

<br>

```c
char password[100];
	sprintf(password, "minihomenest%s", getlogin());
	if(fork() == 0) {
		char *argv[] = {"zip", "-qrP", password, "hasil.zip", "hasil", NULL};
		execv("/usr/bin/zip", argv);
	}
	wait(NULL);
```
- `sprintf(password, "minihomenest%s", getlogin())` untuk menyimpan format password sesuai dengan ketentuan pada variabel *password* dimana fungsi `getlogin()` diperoleh untuk mendapatkan username linux dari praktikan
- `char *argv[] = {"zip", "-qrP", password, "hasil.zip", "hasil", NULL}` digunakan untuk memerintahkan program agar menjalankan zip pada direktori hasil dengan password sesuai ketentuan soal dengan output bernama hasil.zip
- `execv("/usr/bin/zip", argv)` untuk mengeksekusi perintah zip file

#### Output 1d
![output_1d](img/1d.png)

### Soal 1e
Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.

<br>

Untuk melakukan unzip dan membuat file no.txt dibuat fungsi `e` sebagai berikut:
```c
void* e(void *arg)
{
	char password[100];
	sprintf(password, "minihomenest%s", getlogin());
	FILE *file;
	char *argv1[] = {"unzip", "-qo", "-P", password, "hasil.zip", NULL};
	pthread_t id = pthread_self();
	if(pthread_equal(id, tid[0])) {
		if (fork() == 0) {
            execv("/usr/bin/unzip", argv1);
        }
	}
	else if(pthread_equal(id, tid[1])) {
        file = fopen("no.txt", "a");
		fprintf(file, "No");
		fclose(file);
	}
	return NULL;
}
```
Pertama, deklarasi pointer variabel *argv1* untuk melakukan unzip dari file **hasil.zip** yang telah di password sesuai ketentuan soal. Setelah itu, digunakan fungsi `pthread_self()` untuk membuat unique identifier pada setiap thread id. Kemudian, pada statement if akan berjalan di thread id pertama yang digunakan untuk melakukan unzip. Selanjutnya, statement else if akan berjalan di thread id kedua untuk membuat file no.txt yang diisi dengan 'No' menggunakan fungsi `fprintf(file, "No")`.

<br>

```c
while(y<2)
	{
		err=pthread_create(&(tid3[y]), NULL, &e, NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
		y++;
	}
```
Untuk memanggil fungsi `e` tadi, digunakan while loop agar dapat melakukan iterasi pada setiap pembuatan thread yang berhasil.

<br>

```c
if(fork() == 0) {
		char *argv[] = {"zip", "-qrP", password, "hasil.zip", "./hasil", "./no.txt", NULL};
		execv("/usr/bin/zip", argv);
	}
	wait(NULL)
```
Untuk melakukan zip berpassword pada folder hasil sama seperti soal 1d.


#### Output 1e

> Isi hasil.zip

![output_1e_hasil](img/1e_hasil.png)

> Isi no.txt

![output_1e_no](img/1e_no.png)

# Kendala pengerjaan
### Soal 1
- Ketika program dijalankan seringkali menampilkan error pada terminal yang tidak berpengaruh apapun sebagai berikut:
![kendala](img/kendala.png)
