#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <memory.h>
#include <dirent.h>
#include <errno.h>

char base46_map[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                     'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                     'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                     'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

char* base64_decode(char* cipher) {

    char counts = 0;
    char buffer[4];
    char* plain = malloc(strlen(cipher) * 3 / 4);
    int i = 0, p = 0;

    for(i = 0; cipher[i] != '\0'; i++) {
        char k;
        for(k = 0 ; k < 64 && base46_map[k] != cipher[i]; k++);
        buffer[counts++] = k;
        if(counts == 4) {
            plain[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if(buffer[2] != 64)
                plain[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if(buffer[3] != 64)
                plain[p++] = (buffer[2] << 6) + buffer[3];
            counts = 0;
        }
    }

    plain[p] = '\0';    /* string padding character */
    return plain;
}

pthread_t tid[3];
pthread_t tid2[3];
pthread_t tid3[3];

void* unzip(void *arg)
{
	char *argv1[] = {"unzip", "-qo", "music.zip", "-d", "./music/", NULL};
	char *argv2[] = {"unzip", "-qo", "quote.zip", "-d", "./quote/", NULL};
	pthread_t id = pthread_self();
	if(pthread_equal(id, tid[0])) {
		if (fork() == 0) {
            execv("/usr/bin/unzip", argv1);
        }
		wait(NULL);
	}
	else if(pthread_equal(id, tid[1])) {
        if (fork() == 0) {
            execv("/usr/bin/unzip", argv2);
        }
		wait(NULL);
	}
	return NULL;
}

void* decode(void *arg)
{
	pthread_t id = pthread_self();
	if(pthread_equal(id, tid2[0])) {
		DIR *dp;
		struct dirent *ep;
		char path[100];

		sprintf(path, "./music/");

		dp = opendir(path);
		FILE *file, *txt;

		if (dp != NULL)
		{
		while ((ep = readdir (dp))) {
			char str[100] = "";
			char filePath[1000] = "";
			if(strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
				sprintf(filePath, "./music/%s", ep->d_name);
				file = fopen(filePath, "r");
				while(fgets(str, 100, file) != NULL) {
					txt = fopen("music.txt", "a");
					fprintf(txt, "%s\n", base64_decode(str));
					fclose(txt);
				}
				fclose(file);	
			}
		}

		(void) closedir (dp);
		} else perror ("Couldn't open the directory");
	}
	else if(pthread_equal(id, tid2[1])) {
        DIR *dp;
		struct dirent *ep;
		char path[100];

		sprintf(path, "./quote/");

		dp = opendir(path);
		FILE *file, *txt;

		if (dp != NULL)
		{
		while ((ep = readdir (dp))) {
			char str[100] = "";
			char filePath[1000] = "";
			if(strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
				sprintf(filePath, "./quote/%s", ep->d_name);
				file = fopen(filePath, "r");
				while(fgets(str, 100, file) != NULL) {
					txt = fopen("quote.txt", "a");
					fprintf(txt, "%s\n", base64_decode(str));
					fclose(txt);
				}
				fclose(file);	
			}
		}

		(void) closedir (dp);
		} else perror ("Couldn't open the directory");
	}
	return NULL;
}


void* e(void *arg)
{
	char password[100];
	sprintf(password, "minihomenest%s", getlogin());
	FILE *file;
	char *argv1[] = {"unzip", "-qo", "-P", password, "hasil.zip", NULL};
	pthread_t id = pthread_self();
	if(pthread_equal(id, tid[0])) {
		if (fork() == 0) {
            execv("/usr/bin/unzip", argv1);
        }
	}
	else if(pthread_equal(id, tid[1])) {
        file = fopen("no.txt", "a");
		fprintf(file, "No");
		fclose(file);
	}
	return NULL;
}


int main(void)
{
	if(fork() == 0) {
		char *argv[] = {"mkdir", "-p", "music", NULL};
		execv("/usr/bin/mkdir", argv);
	}

	if(fork() == 0) {
		char *argv[] = {"mkdir", "-p", "quote", NULL};
		execv("/usr/bin/mkdir", argv);
	}

	if(fork() == 0) {
		char *argv[] = {"mkdir", "-p", "hasil", NULL};
		execv("/usr/bin/mkdir", argv);
	}
	wait(NULL);

	if(fork() == 0) {
		char *argv[] = {"wget", "-q", "--no-check-certificate", "https://drive.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1", "-O", "music.zip", NULL};
		execv("/usr/bin/wget", argv);
	}
	wait(NULL);

	if(fork() == 0) {
		char *argv[] = {"wget", "-q" ,"--no-check-certificate", "https://drive.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt", "-O", "quote.zip", NULL};
		execv("/usr/bin/wget", argv);
	}
	wait(NULL);

	sleep(3);
	int i=0;
	int x=0;
	int y=0;
	int err;
	while(i<2)
	{
		err=pthread_create(&(tid[i]), NULL, &unzip, NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
		i++;
	}
	pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);

	sleep(3);
	while(x<2)
	{
		err=pthread_create(&(tid2[x]), NULL, &decode, NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
		x++;
	}
	pthread_join(tid2[0],NULL);
	pthread_join(tid2[1],NULL);

	if(fork() == 0) {
		char *argv[] = {"mv", "music.txt", "hasil/music.txt", NULL};
		execv("/usr/bin/mv", argv);
	}
	wait(NULL);

	wait(NULL);
	if(fork() == 0) {
		char *argv[] = {"mv", "quote.txt", "hasil/quote.txt", NULL};
		execv("/usr/bin/mv", argv);
	}
	wait(NULL);

	char password[100];
	sprintf(password, "minihomenest%s", getlogin());
	if(fork() == 0) {
		char *argv[] = {"zip", "-qrP", password, "hasil.zip", "hasil", NULL};
		execv("/usr/bin/zip", argv);
	}
	wait(NULL);

	sleep(3);

	while(y<2)
	{
		err=pthread_create(&(tid3[y]), NULL, &e, NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
		y++;
	}
	pthread_join(tid3[0], NULL);
	pthread_join(tid3[1], NULL);

	if(fork() == 0) {
		char *argv[] = {"zip", "-qrP", password, "hasil.zip", "./hasil", "./no.txt", NULL};
		execv("/usr/bin/zip", argv);
	}
	wait(NULL);


	exit(0);
	return 0;
}